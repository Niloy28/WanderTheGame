using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Wander.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private Transform playerModel;
        [SerializeField] private Camera camera;

        [Range(0f, 10f)][SerializeField] private float playerWalkSpeed = 6f;
        [Range(5f, 20f)][SerializeField] private float playerRunSpeed = 13f;
        [Range(10f, 30f)][SerializeField] private float playerSprintSpeed = 20f;

        [Range(0f, 10f)][SerializeField] private float jumpHeight = 6f;
        [Header("Handle Player Physics")]
        [SerializeField] private float gravity = -9.81f;

        private CharacterController characterController;
        private PlayerAnimationController _animationController;
        private Vector3 rawData;
        private Vector3 lookData;
        private Vector3 gravityForce;

        private bool isSprinting;
        private bool isWalking;

        private void Awake() => CacheReferences();

        private void CacheReferences()
        {
            characterController = GetComponent<CharacterController>();
            _animationController = GetComponent<PlayerAnimationController>();
        }

        private void Update()
        {
            if (characterController.isGrounded)
            {
                MovePlayer();
            }
            _animationController.AnimateMovement(characterController.velocity.magnitude);
        }

        private void LateUpdate()
        {
            if (characterController.isGrounded)
            {
                ResetGravityForce();
            }
            else
            {
                ApplyGravity();
            }
        }

        private void ResetGravityForce() => gravityForce = Vector3.zero;

        private void MovePlayer()
        {
            var moveData = rawData.x * camera.transform.right + rawData.y * camera.transform.forward;
            SavePlayerLookDirection(moveData);

            float moveSpeed;
            if (isSprinting)
            {
                moveSpeed = playerSprintSpeed;
            }
            else if (isWalking)
            {
                moveSpeed = playerWalkSpeed;
            }
            else
            {
                moveSpeed = playerRunSpeed;
            }
            moveData.Normalize();
            characterController.Move(moveSpeed * Time.deltaTime * moveData);
            playerModel.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(lookData), 10f);
        }

        private void SavePlayerLookDirection(Vector3 moveData)
        {
            if (moveData.magnitude != 0)
            {
                lookData = moveData;
            }
            else
            {
                lookData.y = 0;
            }
        }

        private void ApplyGravity()
        {
            gravityForce.y += gravity * Mathf.Pow(Time.fixedDeltaTime, 1);
            characterController.Move(gravityForce);
        }

        public void ReadRawMovementData(InputAction.CallbackContext ctx) => rawData = ctx.ReadValue<Vector2>();

        public void SetPlayerToRun(InputAction.CallbackContext _) => isSprinting = isWalking = false;

        public void SetPlayerToSprint(InputAction.CallbackContext _) => isSprinting = true;

        public void SetPlayerToWalk(InputAction.CallbackContext _) => isWalking = true;

        public void StopPlayerMovement(InputAction.CallbackContext _) => rawData = Vector3.zero;

        public void PlayerJump(InputAction.CallbackContext _)
        {
            if (characterController.isGrounded)
            {
                gravityForce.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
                characterController.Move(gravityForce);
                Invoke(nameof(ResetGravityForce), 0.1f);
            }
        }
    }
}
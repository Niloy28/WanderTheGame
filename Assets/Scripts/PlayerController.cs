using UnityEngine;
using Wander.Input;

namespace Wander.Player
{
    [RequireComponent(typeof(PlayerMovement))]
    public class PlayerController : MonoBehaviour
    {
        private PlayerInputs inputs;
        private PlayerMovement playerMovement;

        private void Awake()
        {
            CacheReferences();
            SetupInputs();
        }

        private void CacheReferences()
        {
            playerMovement = GetComponent<PlayerMovement>();
        }

        private void SetupInputs()
        {
            inputs = new PlayerInputs();

            inputs.Movement.Move.performed += playerMovement.ReadRawMovementData;
            inputs.Movement.Move.canceled += playerMovement.StopPlayerMovement;
            inputs.Movement.Sprint.performed += playerMovement.SetPlayerToSprint;
            inputs.Movement.Sprint.canceled += playerMovement.SetPlayerToRun;
            inputs.Movement.Walk.performed += playerMovement.SetPlayerToWalk;
            inputs.Movement.Walk.canceled += playerMovement.SetPlayerToRun;
            inputs.Movement.Jump.performed += playerMovement.PlayerJump;
        }

        public void DisableInputs()
        {
            inputs.Disable();
        }

        private void OnEnable() => inputs.Enable();

        private void OnDisable() => inputs.Disable();
    }
}





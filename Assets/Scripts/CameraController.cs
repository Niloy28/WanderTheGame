using UnityEngine;
using UnityEngine.InputSystem;
using Wander.Input;

namespace Wander
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private new Camera camera;
        [SerializeField] private int maxFOV = 90;
        [SerializeField] private int minFOV = 40;
        [SerializeField] private Transform player;

        private PlayerInputs cameraControls;

        private void Awake()
        {
            SetupCameraControls();
        }

        private void SetupCameraControls()
        {
            cameraControls = new PlayerInputs();

            cameraControls.Camera.Look.performed += RotateCamera;
            cameraControls.Camera.Zoom.performed += ZoomCamera;
        }

        private void ZoomCamera(InputAction.CallbackContext ctx)
        {
            var scroll = ctx.ReadValue<float>();

            if (scroll > 0.01f)
            {
                camera.fieldOfView--;
            }
            else if (scroll < 0.01f)
            {
                camera.fieldOfView++;
            }
            camera.fieldOfView = Mathf.Clamp(camera.fieldOfView, minFOV, maxFOV);
        }

        private void RotateCamera(InputAction.CallbackContext ctx)
        {
            var rotateData = ctx.ReadValue<Vector2>();
            camera.transform.RotateAround(player.position, Vector3.up, rotateData.x);
        }

        private void OnEnable()
        {
            cameraControls.Enable();
        }

        private void OnDisable()
        {
            cameraControls.Disable();
        }
    }
}
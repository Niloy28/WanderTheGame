using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wander.Player
{
    public class PlayerAnimationController : MonoBehaviour
    {
        private Animator _animator;

        private void Start()
        {
            _animator = GetComponent<Animator>();
        }

        public void AnimateMovement(float velocity)
        {
            _animator.SetFloat("velocity", velocity);
        }
    }
}